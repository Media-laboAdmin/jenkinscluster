# JenkinsCluster

Master/Slave構成のJenkins環境を構築するためのファイル一式です。

次の3種類の仮想マシンで構成されます。

* controller
    * Jenkins構築の構築や状況確認などのコマンドを実行するための仮想マシン。
* master
    * JenkinsのMasterノードを実行する仮想マシン。非仮想マシンでも可。
* slave
    * JenkinsのSlaveノードを実行する仮想マシン。非仮想マシンでも可。


## 必要環境

次のソフトウェアがインストールされた環境で実行してください。
OSはなんでも大丈夫なはず。

* Git
* sshが使えるターミナル
* Vagrant
* VirtualBox

動作確認を行ったときのバージョンは以下の通り

* OS
    * macOS Sierra バージョン10.12.2
* ターミナル
    * OS付属のもの
* Git
    * バージョン2.6.4
* Vagrant
    * バージョン1.9.0
* VirtualBox
    * バージョン5.1.2


## 作成手順

以下の手順で作成します。

1. リポジトリを任意の場所にクローン。

2. ターミナルを開きクローンした場所に移動。

3. Vagrantのコマンドで仮想マシン作成。

    * controllerだけを作成する場合は、`vagrant up controller`

    * 3種類とも作成する場合は、`vagrant up` もしくは `vagrant up controller master slave`

    * 初回実行時は仮想マシンの元になるBoxファイル（数百MB〜十数GB）のダウンロードが行われる。

4. controllerからmaster/slaveにssh接続できるようにする。

    1. controller上で秘密鍵と公開鍵を生成。

        * `ssh-keygen -t rsa`

        * 途中で入力待ちになるが、何も入力せずEnterでOK。

    2. 公開鍵の中身を開いてマウスで選択してクリップボードにコピー。

        * `less ~/.ssh/id_rsa.pub`

    3. masterとslave上のauthorized_keysにcontrollerの公開鍵の内容を追記。

        * authorized_keysは接続ユーザの`~/.ssh/authorized_keys`

        * authorized_keysが既に存在するなら追記。存在しないならファイルを新規作成すること。

    4. controllerからmasterとslaveにssh接続できればOK。

5. masterとslaveの環境設定を行う。

    1. controllerに接続。

        * `vagrant ssh controller`

    2. `/vagrant`ディレクトリに移動。

        * `cd /vagrant`

    3. masterとslaveの接続情報を `/vagrant/playbook/hosts` に記載。

        * 仮想マシンのIPを変更したり実マシンを使用したりする場合は、接続情報を変更してください。

    4. 環境設定スクリプトを実行。

        * `sh ./setup.sh`

        * masterもslaveも設定したい場合は、スクリプト起動後のメニューで`all`を入力。

        * masterもしくはslaveだけを設定したい場合は、スクリプト起動後のメニューで`master`もしくは`slave`を入力。

        * 起動後のメニューではなくてスクリプトの引数でmasterやslaveを指定してもOK。

6. Jenkinsの設定を行う。

    1. ブラウザでJenkinsに接続。

        * URLは、`http://masterのipアドレス`

        * 初期設定のままなら、http://192.168.33.20

    2. 画面に表示されてる`initialAdminPassword`のパスをメモ。

    3. ターミナルでmasterノードにssh接続。

        * 仮想マシンを使ってる場合は、`vagrant ssh master`で接続可能。

    4. メモしたパスの`/var/jenkins_home`の部分を`/var/lib/jenkins`に置き換えた場所にあるファイルの中身の文字列をメモ。

    5. メモした文字列をブラウザに入力しcontinueボタン押下。

    6. `Install suggested plugins`ボタン押下。おすすめプラグインがインストールされます。

        * プラグインを入れたくないなら右上の×ボタン押下。

        * インストール終わったのに先に進まないときは、ブラウザで再読込してください。

    7. 管理者ユーザーの入力画面が出るので入力。

    8. 画面の指示通りに進んで、「Jenkinsへようこそ！」が表示されたらOK。

7. JenkinsのSlaveノードの設定を行う。

    Slave仮想マシン上で動くDockerコンテナをSlaveノードとして使用するための設定を行います。

    1. Jenkinsのプラグイン`Docker Plugin`をインストール。

        * Jenkinsの管理 → プラグインの管理 → 利用可能タブ

        * 失敗することが多いみたいなので、何度も挑戦してみること。

    2. Jenkinsのトップ画面から、「Jenkinsの管理」→「システムの設定」を開く。

    3. クラウドの項目の、「追加」→「docker」を選択して下記を入力。

        * DockerURL ･･･ tcp://slaveのIPアドレス:2475

            * 設定をいじってないなら tpc://192.168.33.30:2475

    4. 「TestConnection」ボタンを押下。赤い文字が表示されないことを確認すること。

    5. 残りの項目を入力して保存。

        * Images → ビルドで使用するDockerイメージ。最低一つは入力すること。

            * JDK8入りのサンプル

                * Docker Image → canpok1/jenkins-slave:jdk8

                * 認証情報 → ユーザー名もパスワードもjenkins

    6. 任意のジョブを作成→ビルドして、ビルドが通れば構築完了。


## 更新手順

定期的にJenkinsのバージョンアップが行われますので、次の手順で更新してください。

1. Jenkinsのmasterノードにssh接続。

2. `docker-compose.yml`の配置場所に移動。

3. 最新のDockerイメージを取得。

    ```
    docker-compose pull
    ```
    
4. Jenkinsにブラウザでアクセスしてシャットダウン準備にする。（Jenkinsの管理 → シャットダウンの準備）

5. 実行中の全ビルドが終了したらコンテナを最新化。

    ```
    docker-compose up -d
    ```

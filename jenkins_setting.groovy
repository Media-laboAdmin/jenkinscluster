import jenkins.model.*
import com.nirima.jenkins.plugins.docker.*
import com.nirima.jenkins.plugins.docker.launcher.*
import com.nirima.jenkins.plugins.docker.strategy.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.domains.*
import org.jenkinsci.plugins.plaincredentials.*
import org.jenkinsci.plugins.plaincredentials.impl.*
import hudson.util.Secret

def jenkinsSlaveCredentialsId = 'docker-container-login'
def chatworkCredentialsId = 'chatwork-api'

///////////////////////////////////////////////////:
// 全体設定
///////////////////////////////////////////////////:
def instance = Jenkins.instance;

// 同時ビルド数
def masterNumExecutors = 0;
instance.setNumExecutors(masterNumExecutors);
println "【同時ビルド数】${masterNumExecutors}にセット"

// 環境変数
configureEnvVars([
  [key:"PG_9_4_HOST", value:"192.168.33.30"],
  [key:"PG_9_4_PORT", value:"5432"],
  [key:"PG_9_4_USER", value:"postgres"],
  [key:"PG_9_4_PASS", value:"postgres"],

  [key:"PG_9_5_HOST", value:"192.168.33.30"],
  [key:"PG_9_5_PORT", value:"5433"],
  [key:"PG_9_5_USER", value:"postgres"],
  [key:"PG_9_5_PASS", value:"postgres"],

  [key:"PG_9_6_HOST", value:"192.168.33.30"],
  [key:"PG_9_6_PORT", value:"5434"],
  [key:"PG_9_6_USER", value:"postgres"],
  [key:"PG_9_6_PASS", value:"postgres"],

  [key:"CW_CREDENTIAL_ID", value:chatworkCredentialsId],
], true);


// 認証情報追加
if (hasCredential(jenkinsSlaveCredentialsId)) {
    println "【認証情報】作成済みなのでスキップ [id = ${jenkinsSlaveCredentialsId}]";
} else {
    createGlobalCredential(jenkinsSlaveCredentialsId, "dockerコンテナへのログイン用", "jenkins", "jenkins", CredentialsScope.SYSTEM);
    println "【認証情報】作成 [id = ${jenkinsSlaveCredentialsId}]";
}
if (hasCredential(chatworkCredentialsId)) {
    println "【認証情報】作成済みなのでスキップ [id = ${chatworkCredentialsId}]";
} else {
    createGlobalSecretText(chatworkCredentialsId, "通知用ChatworkアカウントのAPIトークン", "AAAAAAAA", CredentialsScope.GLOBAL);
    println "【認証情報】作成 [id = ${chatworkCredentialsId}]";
}

// クラウド
Jenkins.instance.clouds.clear();
println "【Docker】既存のクラウド設定をクリアしました"
Jenkins.instance.clouds.addAll(makeDockerCloud(
[
    [
        name: 'jenkins-slave',
        serverUrl: "tcp://192.168.33.30:2475",
        containerCapStr: '100',
        connectionTimeout: 5,
        readTimeout: 15,
        credentialsId: "",
        version: '',
        templates: [
            [
                image: 'canpok1/jenkins-slave:jdk8',
                labelString: 'jdk8 linux',
                remoteFs: '',
                credentialsId: jenkinsSlaveCredentialsId,
                idleTerminationMinutes: '5',
                sshLaunchTimeoutMinutes: '1',
                jvmOptions: "-Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8",
                javaPath: '',
                memoryLimit: 2500,
                memorySwap: 0,
                cpuShares: 0,
                prefixStartSlaveCmd: '',
                suffixStartSlaveCmd: '',
                instanceCapStr: '5',
                dnsString: '',
                network: '',
                dockerCommand: '',
                volumesString: '',
                volumesFromString: '',
                hostname: '',
                bindPorts: '',
                bindAllPorts: false,
                privileged: false,
                tty: false,
                macAddress: ''
            ],
            [
                image: 'canpok1/jenkins-slave:jdk9',
                labelString: 'jdk9 linux',
                remoteFs: '',
                credentialsId: jenkinsSlaveCredentialsId,
                idleTerminationMinutes: '5',
                sshLaunchTimeoutMinutes: '1',
                jvmOptions: "-Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8",
                javaPath: '',
                memoryLimit: 2500,
                memorySwap: 0,
                cpuShares: 0,
                prefixStartSlaveCmd: '',
                suffixStartSlaveCmd: '',
                instanceCapStr: '5',
                dnsString: '',
                network: '',
                dockerCommand: '',
                volumesString: '',
                volumesFromString: '',
                hostname: '',
                bindPorts: '',
                bindAllPorts: false,
                privileged: false,
                tty: false,
                macAddress: ''
            ],
            [
                image: 'canpok1/jenkins-slave:node6.9.5',
                labelString: 'node6.9.5 linux',
                remoteFs: '',
                credentialsId: jenkinsSlaveCredentialsId,
                idleTerminationMinutes: '5',
                sshLaunchTimeoutMinutes: '1',
                jvmOptions: "-Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8",
                javaPath: '',
                memoryLimit: 2500,
                memorySwap: 0,
                cpuShares: 0,
                prefixStartSlaveCmd: '',
                suffixStartSlaveCmd: '',
                instanceCapStr: '5',
                dnsString: '',
                network: '',
                dockerCommand: '',
                volumesString: '',
                volumesFromString: '',
                hostname: '',
                bindPorts: '',
                bindAllPorts: false,
                privileged: false,
                tty: false,
                macAddress: ''
            ],
            [
                image: 'canpok1/jenkins-slave:node8.4',
                labelString: 'node8.4 linux',
                remoteFs: '',
                credentialsId: jenkinsSlaveCredentialsId,
                idleTerminationMinutes: '5',
                sshLaunchTimeoutMinutes: '1',
                jvmOptions: "-Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8",
                javaPath: '',
                memoryLimit: 2500,
                memorySwap: 0,
                cpuShares: 0,
                prefixStartSlaveCmd: '',
                suffixStartSlaveCmd: '',
                instanceCapStr: '5',
                dnsString: '',
                network: '',
                dockerCommand: '',
                volumesString: '',
                volumesFromString: '',
                hostname: '',
                bindPorts: '',
                bindAllPorts: false,
                privileged: false,
                tty: false,
                macAddress: ''
            ]
        ]
    ]
]))

instance.save();





/**
 * 認証情報が登録済みであるかを判定します.
 * @param id 認証情報のID
 * @return {boolean} 登録済みならtrue, 登録済みでないならfalse.
 */
def hasCredential(id) {
  def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
    com.cloudbees.plugins.credentials.common.StandardUsernameCredentials.class,
    Jenkins.instance
  )
  def c = creds.findResult { it.id == id ? it : null }
  return c != null
}

/**
 * Dockerの設定を組み立てます.
 * @param dockerSettings 設定値
 * @return 組み立てた設定
 */
def makeDockerCloud(dockerSettings) {
  def dockerClouds = []
  dockerSettings.each { cloud ->
    println "【Docker】クラウド追加 [${cloud.name}]"
  
    def templates = []
    cloud.templates.each { template ->
        println "【Docker】テンプレート追加 [${template.image}]"
        def dockerTemplateBase =
            new DockerTemplateBase(
               template.image,
               template.dnsString,
               template.network,
               template.dockerCommand,
               template.volumesString,
               template.volumesFromString,
               template.environmentsString,
               template.lxcConfString,
               template.hostname,
               template.memoryLimit,
               template.memorySwap,
               template.cpuShares,
               template.bindPorts,
               template.bindAllPorts,
               template.privileged,
               template.tty,
               template.macAddress
        )
  
        def dockerTemplate =
          new DockerTemplate(
            dockerTemplateBase,
            template.labelString,
            template.remoteFs,
            template.remoteFsMapping,
            template.instanceCapStr
          )

        def dockerComputerSSHLauncher = new DockerComputerSSHLauncher(
            new hudson.plugins.sshslaves.SSHConnector(22, template.credentialsId, null, null, null, null, null )
        )
        dockerTemplate.setLauncher(dockerComputerSSHLauncher)
  
        dockerTemplate.setMode(Node.Mode.NORMAL)
        dockerTemplate.setNumExecutors(1)
        dockerTemplate.setRemoveVolumes(true)
        dockerTemplate.setRetentionStrategy(new DockerOnceRetentionStrategy(10))
        //dockerTemplate.setPullStrategy(DockerImagePullStrategy.PULL_LATEST)
        dockerTemplate.setPullStrategy(DockerImagePullStrategy.PULL_ALWAYS)
  
        templates.add(dockerTemplate)
    }
  
    dockerClouds.add(
      new DockerCloud(cloud.name,
                      templates,
                      cloud.serverUrl,
                      cloud.containerCapStr,
                      cloud.connectTimeout ?: 15, // Well, it's one for the money...
                      cloud.readTimeout ?: 15,    // Two for the show
                      cloud.credentialsId,
                      cloud.version
      )
    )
  }
  return dockerClouds;
}


/**
 * 認証情報を追加します。
 * @param {String} id ID
 * @param {String} memo メモ
 * @param {String} username ユーザー名
 * @param {String} password パスワード
 * @param {Object} scope スコープ。CredentialsScope.SYSTEM もしくは GLOBAL もしくは USER を指定。
 */
def createGlobalCredential(id, memo, username, password, scope) {
  def system_creds = SystemCredentialsProvider.getInstance()
  Map<Domain, List<Credentials>> domainCredentialsMap = system_creds.getDomainCredentialsMap()
  domainCredentialsMap[Domain.global()].add(
    new UsernamePasswordCredentialsImpl(
      scope,
      id,
      memo,
      username,
      password
      )
  )
}

/**
 * 認証用文字列を追加します。
 * @param {String} id ID
 * @param {String} memo メモ
 * @param {String} text 文字列
 * @param {Object} scope スコープ。CredentialsScope.SYSTEM もしくは GLOBAL もしくは USER を指定。
 */
def createGlobalSecretText(id, memo, text, scope) {
  def system_creds = SystemCredentialsProvider.getInstance()
  Map<Domain, List<Credentials>> domainCredentialsMap = system_creds.getDomainCredentialsMap()
  domainCredentialsMap[Domain.global()].add(
    new StringCredentialsImpl(
      scope,
      id,
      memo,
      Secret.fromString(text)
      )
  )
}


/**
 * 環境変数を追加します。
 * @param keyValueList 環境変数
 * @param shouldClear 既存の環境変数を初期化するか
 */
def configureEnvVars(keyValueList, shouldClear) {
  globalNodeProperties = Jenkins.instance.getGlobalNodeProperties();
  envVarsNodePropertyList = globalNodeProperties.getAll(hudson.slaves.EnvironmentVariablesNodeProperty.class);
  
  envVars = null;
  if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
    newEnvVarsNodeProperty = new hudson.slaves.EnvironmentVariablesNodeProperty();
    globalNodeProperties.add(newEnvVarsNodeProperty);
    envVars = newEnvVarsNodeProperty.getEnvVars();
  } else {
    envVars = envVarsNodePropertyList.get(0).getEnvVars();
  }
  
  if (shouldClear) {
    envVars.clear();
    println "【環境変数】既存の環境変数をクリアしました"
  }
  
  keyValueList.each { keyValue ->
    envVars.put(keyValue.key, keyValue.value);
    println "【環境変数】追加 [key=${keyValue.key}, value=${keyValue.value}]"
  }
}

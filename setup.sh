#!/bin/sh

# ${0} の dirname を取得
cwd=`dirname "${0}"`
# ${0} が 相対パスの場合は cd して pwd を取得
expr "${0}" : "/.*" > /dev/null || cwd=`(cd "${cwd}" && pwd)`

GROUP_NAME=${1}
if [ -z "$GROUP_NAME" ]; then
    echo "*******************************************"
    echo "Please enter setup group name."
    echo "if name is empty, setup all groups."
    echo "*******************************************"
    read GROUP_NAME
    
    if [ -z "$GROUP_NAME" ]; then
        GROUP_NAME=all
    fi
fi

ansible-galaxy install --roles-path ${cwd}/playbook/roles -r ${cwd}/playbook/requirements.yml
ansible-playbook -i ${cwd}/playbook/hosts -l $GROUP_NAME ${cwd}/playbook/setup.yml

#!/bin/bash -eu

SCRIPT_DIR={{jenkins_backup.workspace}}/backup-script
SCRIPT_PATH=${SCRIPT_DIR}/jenkins-backup.sh
BACKUP_FILE={{jenkins_backup.workspace}}/backup.tar.gz
MOUNTPOINT={{jenkins_backup.nas.mountpoint}}
REMOTE_PATH={{jenkins_backup.nas.path}}
CREDENTIALS={{jenkins_backup.workspace}}/credentials.txt

if [ ! -e ${SCRIPT_PATH} ]; then
    git clone -b 0.1.8 --single-branch https://github.com/sue445/jenkins-backup-script.git ${SCRIPT_DIR}
fi
${SCRIPT_PATH} {{jenkins_backup.jenkins_home}} ${BACKUP_FILE}

if [ ! -z ${REMOTE_PATH} ]; then
    if [ ! -e ${MOUNTPOINT} ]; then
        sudo mkdir ${MOUNTPOINT}
        sudo chmod 777 ${MOUNTPOINT}
    fi
    sudo mount -t cifs -o uid=$(id -u),gid=$(id -g),credentials=${CREDENTIALS} ${REMOTE_PATH} ${MOUNTPOINT}
    cp ${BACKUP_FILE} ${MOUNTPOINT}
    sudo umount ${MOUNTPOINT}
fi
